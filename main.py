import time

import cv2
from matplotlib import pyplot as plt
from mss import mss
import numpy as np
import pyautogui
from skimage.morphology import binary_closing, binary_dilation, binary_erosion, binary_opening
import threading
from pynput.keyboard import Key, Controller

pyautogui.PAUSE = 0.001

action = 0

keyboard = Controller()


def current_milli_time():
    return round(time.time() * 1000)


def find_cactus(img):
    global action
    global xi
    if 0 in img[y, int(s):int(xi)]:
        action = 1


def find_bird(img):
    global action
    global bird_detected
    global xi
    if 0 in img[y_bird, int(s):int(xi)]:
        bird_detected = current_milli_time()
        action = 2


def pres_up():
    pyautogui.press('up')
    #keyboard.release('up')


def pres_down():
    pyautogui.keyDown("down")


type1 = np.ones((5, 5))

monitor = {"top": 185, "left": 157, "width": 625, "height": 196}

time.sleep(3)

pyautogui.press('up')

y = 130

x = 145

s = 100
y_bird = 90

started = current_milli_time()
bird_detected = -1

while True:
    with mss() as sct:
        img = np.array(sct.grab(monitor))

    im_gray_th_otsu = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    th, im_gray_th_otsu = cv2.threshold(im_gray_th_otsu, 128, 192, cv2.THRESH_OTSU)
    # im_gray_th_otsu = binary_closing(im_gray_th_otsu, type1)
    # im_gray_th_otsu = im_gray_th_otsu.astype(np.uint8)
    # im_gray_th_otsu[im_gray_th_otsu > 0] = 255
    xi = (current_milli_time() - started) * 0.0008 + x
    # xi = x

    # find_bird(im_gray_th_otsu)
    # find_cactus(im_gray_th_otsu)
    th1 = threading.Thread(target=find_cactus, args=(im_gray_th_otsu,))
    th2 = threading.Thread(target=find_bird, args=(im_gray_th_otsu,))
    th1.start()
    th2.start()

    th1.join()
    th2.join()

    if current_milli_time() - bird_detected < 1000:
        action = 2
    else:
        pyautogui.keyUp('down')
        bird_detected = -1

    if action == 0:
        pyautogui.keyUp('down')
    if action == 1:
        pres_up()
    elif action == 2:
        pres_down()

    action = 0

    cv2.circle(im_gray_th_otsu, (int(xi), y), 5, (0, 0, 255), 2)
    cv2.imshow("Dino", im_gray_th_otsu)

    key = cv2.waitKey(1)
    if key == ord('1'):
        pyautogui.keyUp("up")
        pyautogui.keyUp("down")
        break
    elif key == ord('2'):
        started = current_milli_time()
cv2.destroyAllWindows()
